# Authentication microservice of DrMarkdown project

## ***Technologies used:***

* **Microservice architecture**
* **Java 11**
* **SpringBoot**
* **Angular 9**
* **Google Kubernetes cluster (GKE)**
* **Process automation using CI/CD pipelines from GitLab**
* **Spring Security/JWT Token**
* **MongoDB Atlas**
* **Guava**
* **Apache Commons**
* **Gradle**
* **Connection Security between frontend and backend using TLS (https - encrypted communication)**
* **Docker images**

<p align = "center">
<img src="https://gitlab.com/drmarkdown-third/drmarkdown-auth/-/raw/master/images/mark_01.JPG">
</p>

<p align = "center">
<img src="https://gitlab.com/drmarkdown-third/drmarkdown-auth/-/raw/master/images/mark_02.JPG">
</p>

<p align = "center">
<img src="https://gitlab.com/drmarkdown-third/drmarkdown-auth/-/raw/master/images/mark_03.JPG"> 
</p>

<p align = "center">
<img src="https://gitlab.com/drmarkdown-third/drmarkdown-auth/-/raw/master/images/mark_04.JPG">
</p>

<p align = "center">
<img src="https://gitlab.com/drmarkdown-third/drmarkdown-auth/-/raw/master/images/mark_05.JPG">
</p>

<p align = "center">
<img src="https://gitlab.com/drmarkdown-third/drmarkdown-auth/-/raw/master/images/mark_06.JPG"> 
</p>
