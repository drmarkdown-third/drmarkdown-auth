package com.drmarkdown.auth.services;

import com.drmarkdown.auth.dtos.UserInfoDTO;
import com.drmarkdown.auth.dtos.UserLoginDTO;

public interface UserService {

    //     user creation
    void createUser(UserInfoDTO userInfoDTO);

    //    user fetching
    UserInfoDTO retrieveUserInfo(String userId, String token);

    //    user login
    UserInfoDTO loginUser(UserLoginDTO userLoginDTO);
}
