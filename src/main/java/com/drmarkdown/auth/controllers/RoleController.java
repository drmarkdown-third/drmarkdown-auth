package com.drmarkdown.auth.controllers;

import com.drmarkdown.auth.dtos.RoleDTO;
import com.drmarkdown.auth.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
@RestController
@RequestMapping("/role")
@PreAuthorize("hasAnyRole('ADMIN')")
public class RoleController {

    @Autowired
    private RoleService roleService;

    // - create role
    @PostMapping("/create")
    public RoleDTO createRole(@RequestBody RoleDTO roleDTO) {

        checkNotNull(roleDTO);
        roleService.createRole(roleDTO);

        return roleDTO;
    }

// - get info about a specific role
    @GetMapping("/info/{roleId}")
    public RoleDTO getRoleInfo(@PathVariable String roleId) {

        return roleService.roleInfo(roleId);
    }

//- delete a role
    //- modify a role
    // @DeleteMapping("/{id}")
    // public void deleteRole(@PathVariable String id) {

    //    log.info("deleting role by id: " + id);
    // }

    // @PutMapping("/{id}")
    // public RoleDTO updateRole(@PathVariable String id, @RequestBody RoleDTO roleDTO) {

    //    log.info("updating user with id: " + id);
    //    return null;
    // }
}