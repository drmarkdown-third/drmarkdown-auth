package com.drmarkdown.auth.controllers;

import com.drmarkdown.auth.dtos.RoleDTO;
import com.drmarkdown.auth.dtos.UserInfoDTO;
import com.drmarkdown.auth.dtos.UserLoginDTO;
import com.drmarkdown.auth.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    //    - create user
    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ANONYMOUS', 'ADMIN')")
    public UserInfoDTO createUser(@RequestBody UserInfoDTO userInfoDTO) {

        checkNotNull(userInfoDTO);

        userService.createUser(userInfoDTO);

        return userInfoDTO;
    }

    //- get info about a specific user
    @GetMapping("/info/{userId}")
    @PreAuthorize("hasAnyRole('ANONYMOUS', 'USER', 'ADMIN')")
    public UserInfoDTO getUserInfo(@PathVariable String userId, HttpServletRequest request) {

        String tokenUnstripped = request.getHeader(AUTHORIZATION);
        String token = null;
        if(!isEmpty(tokenUnstripped)) {
            token = StringUtils.removeStart(tokenUnstripped, "Bearer").trim();
        }

        return userService.retrieveUserInfo(userId, token);
    }

    @PostMapping("/login")
    @PreAuthorize("hasAnyRole('ANONYMOUS')")
    public UserInfoDTO loginUser(@RequestBody UserLoginDTO userLoginDTO) {

        checkNotNull(userLoginDTO);

        return userService.loginUser(userLoginDTO);
    }

//- delete a user
    @DeleteMapping("/{id}")
    public void deleteRole(@PathVariable String id) {

        // TODO: homework
        log.info("deleting role by id: " + id);
    }

//- modify a user
    @PutMapping("/{id}")
    public RoleDTO updateRole(@PathVariable String id, @RequestBody RoleDTO roleDTO) {

        log.info("updating user with id: " + id);
        return null;
    }
}